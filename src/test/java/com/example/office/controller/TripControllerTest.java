package com.example.office.controller;

import com.example.office.model.Address;
import com.example.office.model.Client;
import com.example.office.model.MyDate;
import com.example.office.model.Trip;
import com.example.office.repository.TravelOffice;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TripControllerTest {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    @Autowired
    private MockMvc mockMvc;
    private Client testClient;
    private TravelOffice travelOffice;

    @Test
    public void shouldPostUser() throws Exception {
        Client createdClient = createUser();
        assertThat(createdClient.getName()).isNotNull();
        assertThat(createdClient.getTrip()).isNotNull();
        assertThat(createdClient.getAddress()).isNotNull();

    }

    public Client createUser() throws Exception {
        Client client = new Client();
        client.setName("Igor");
        client.setTrip(Trip
                .builder()
                .start(new MyDate(10, 10, 2002))
                .end(new MyDate(10, 10, 2002))
                .price(20)
                .destination("Barcelona").build());
        client.setAddress(new Address("street", "zip", "Lodz"));

        String postValue = OBJECT_MAPPER.writeValueAsString(client);


        MvcResult storyResult = this.mockMvc.perform(
                MockMvcRequestBuilders.post("/addClients")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(postValue))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();

        return OBJECT_MAPPER.readValue(storyResult.getResponse().getContentAsString(), Client.class);
    }


}
