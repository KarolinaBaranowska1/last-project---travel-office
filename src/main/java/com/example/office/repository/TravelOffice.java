package com.example.office.repository;

import com.example.office.model.Client;

import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class TravelOffice
    {
    private Map<Long, Client> map = new HashMap();
    private static Long id = 0L;

    public void addClient(Client client)
        {
        map.put(id++, client);

        }

    public Map<Long, Client> getAllClients()
        {
        return map;
        }
    }
