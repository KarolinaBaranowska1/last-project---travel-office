package com.example.office.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
public class WithinCountryTrip extends Trip
    {
    private Integer ownArrivalDiscount;
    private WithinCountryTrip withinCountryTrip;
    }
