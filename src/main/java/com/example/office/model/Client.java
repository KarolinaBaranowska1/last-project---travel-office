package com.example.office.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Client
    {
    private String name;
    private Address address;
    private Trip trip;


    public String getInfo(Trip trip, Address address, Client client)
        {
        String infoAddres = address.toString();
        String infoTrip = trip.toString();
        String clientName = client.getName();
        String totalInfo = "\n" + "Klient " + clientName + "o adresie  " + infoAddres
                + "\n" + "ma wycieczke:  "
                + "\n" + infoTrip;
        return totalInfo;
        }
    }
