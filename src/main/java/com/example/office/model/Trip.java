package com.example.office.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class Trip
    {
    private MyDate start;
    private MyDate end;
    private String destination;
    private Integer price;
    }
