package com.example.office.controller;

import com.example.office.model.Client;
import com.example.office.repository.TravelOffice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class TripController
    {
    private Client client = Client.builder().name("Adam Kowal").build();
    private Client client2 = Client.builder().name("Ala ma").build();
    private Client client3 = Client.builder().name("Tom Kot").build();
    private Client client4 = Client.builder().name("Tomek Kotek").build();

    @Autowired
    TravelOffice travelOffice;


    @GetMapping("/listClients")
    public Map<Long, Client> getAllClients()
        {
        travelOffice.addClient(client);
        travelOffice.addClient(client2);
        travelOffice.addClient(client3);
        travelOffice.addClient(client4);

        return travelOffice.getAllClients();
        }

    @PostMapping("/addClients")
    public Client addClients(@RequestBody Client thisClient)
        {
        travelOffice.addClient(thisClient);
        return thisClient;
        }
    }
